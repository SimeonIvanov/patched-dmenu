# patched-dmenu

My patched version of dmenu - a run launcher program created by [suckless.org](https://tools.suckless.org/dmenu/)

![Screenshot](screenshot.png)

## Patches:

- [border](https://tools.suckless.org/dmenu/patches/)
- [center](https://tools.suckless.org/dmenu/patches/center/)
